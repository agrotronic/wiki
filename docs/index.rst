.. index:: Индекс, !Главная

.. meta::
    :http-equiv=Content-Type: text/html; charset=utf-8
    :author: AgroDigit
    :description: Документация Агротроник, Агро Цифра
    :keywords: agrodigit, wiki, Agrotronic

.. комментарий
   https://sphinx-ru.readthedocs.io/ru/latest/sphinx-markup.html
   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
  .. toctree::
     :maxdepth: — уровни заголовков, включаемых в оглавление;
     :numbered: — нумерация всех пунктов оглавления;
     :hidden: — позволяет скрыть оглавление.

Документация Агро Цифра
================================

Оглавление:

.. toctree::
   :maxdepth: 1

   kalibrovka
   telematika
   egts
   wialonips
   annex1
   annex2
   annex3
   annex4
   annex5
   annex6
   annex7

Ссылки на внешние ресурсы:

* `AgroDigit <https://agrodigit.ru/>`_


Индексы и таблицы
==================

* :ref:`search`
